<?php
session_start();
require_once dirname(__FILE__).'/autoload.php';
$config = ConfigUtility::getConfig();
AuthUtility::doAuthTokenHandling();
$step = array_key_exists('step', $_REQUEST) ? $_REQUEST['step'] : 'select-playlist';
switch ($step) {
    case 'select-artists':
        $playlistId = $_GET['playlist'];
        try {
            $tracks = SessionUtility::getTracksByPlaylist($playlistId);
            SessionUtility::saveTracksFromPlaylist($tracks, $playlistId);
        } catch (\Exception $e) {
            $tracks = TrackUtility::getByPlaylistId($playlistId);
            SessionUtility::saveTracksFromPlaylist($tracks, $playlistId);
        }
        $artists = ArtistUtility::getArtistsByTracks($tracks);
        usort($artists, function ($a, $b) {
            return strtolower($a['artist']['name']) > strtolower($b['artist']['name']);
        });
        require_once dirname(__FILE__).'/Views/SelectArtists.phtml';
        break;
    case 'create-playlist':
        $tracks = [];
        foreach ($_POST['tracks'] as $trackList) {
            $tracks = array_merge($tracks, explode(',', $trackList));
        }
        $tracks = array_unique($tracks);
        $playlist = CurlUtility::createPlaylist($_POST['playlist-name']);
        foreach(array_chunk($tracks, 50) as $chunkedTrack) {
            $addResponse = CurlUtility::addTracksToPlaylist($chunkedTrack, $playlist);
        }

        require_once dirname(__FILE__).'/Views/CreatePlaylist.phtml';
        break;
    case 'select-playlist':
    default:
        try {
            $playlists = SessionUtility::getPlaylists();
        } catch (\Exception $e) {
            $playlists = PlaylistUtility::getPlaylists();
            usort($playlists, function ($a, $b) {
                return strtolower($a['name']) > strtolower($b['name']);
            });
            SessionUtility::savePlaylists($playlists);
        }
        require_once dirname(__FILE__).'/Views/SelectPlaylist.phtml';
        break;
}

