<?php

class UserUtility
{

    private static $user;

    public static function getUser() {
        if(UserUtility::$user === null) {
            UserUtility::$user = CurlUtility::doRequest('https://api.spotify.com/v1/me');
        }
        return UserUtility::$user;
    }
}