<?php

class SessionUtility
{

    public static function getAccessTokenResponse()
    {
        if (array_key_exists('access_token_response', $_SESSION)) {
            return json_decode($_SESSION['access_token_response'], true);
        } else {
            throw new Exception('access token response not in session');
        }
    }

    public static function saveAccessTokenResponse($tokenResult)
    {
        $_SESSION['access_token_response'] = json_encode($tokenResult);
    }

    public static function getPlaylists()
    {
        if (array_key_exists('playlists', $_SESSION)) {
            return json_decode($_SESSION['playlists'], true);
        } else {
            throw new Exception('playlists not in session');
        }
    }

    public static function savePlaylists($playlists)
    {
        $_SESSION['playlists'] = json_encode($playlists);
    }

    public static function getTracksByPlaylist($playlistId)
    {
        if (array_key_exists('tracks_by_playlist', $_SESSION) && array_key_exists($playlistId,
                $_SESSION['tracks_by_playlist'])) {
            return json_decode($_SESSION['tracks_by_playlist'][$playlistId], true);
        } else {
            throw new Exception('tracks by playlist not in session');
        }
    }

    public static function saveTracksFromPlaylist($tracks, $playlistId)
    {
        if (!array_key_exists('tracks_by_playlist', $_SESSION)) {
            $_SESSION['tracks_by_playlist'] = [];
        }
        if (!array_key_exists($playlistId, $_SESSION['tracks_by_playlist'])) {
            $_SESSION['tracks_by_playlist'][$playlistId] = [];
        }
        $_SESSION['tracks_by_playlist'][$playlistId] = json_encode($tracks);
    }
}