<?php

class CurlUtility
{

    public static function doRequest($url)
    {
        $curl = 'curl -X "GET" "'.$url.'" -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer '.AuthUtility::getAccessToken().'"';

        return json_decode(shell_exec($curl), true);
    }

    public static function doTokenRequest($code)
    {
        $url = 'https://accounts.spotify.com/api/token';
        $authBearer = base64_encode(ConfigUtility::getConfig()->getClientId().':'.ConfigUtility::getConfig()->getClientSecret());
        $redirectUri = ConfigUtility::getConfig()->getBaseUrl();
        $curl = 'curl -H "Authorization: Basic '.$authBearer.'" -d grant_type=authorization_code -d code='.$code.' -d redirect_uri='.$redirectUri.' '.$url;

        return json_decode(shell_exec($curl), true);
    }

    public static function createPlaylist($name)
    {
        $url = 'https://api.spotify.com/v1/me/playlists';
        $curl = 'curl -X "POST" "'.$url.'" --data "{\"name\":\"'.$name.'\",\"description\":\"New playlist description\",\"public\":false}"  -H "Accept: application/json" -H "Content-Type: application/json" -H "Authorization: Bearer '.AuthUtility::getAccessToken().'"';

        return json_decode(shell_exec($curl), true);
    }

    public static function addTracksToPlaylist($tracks, $playlist)
    {
        $url = 'https://api.spotify.com/v1/playlists/';
        foreach ($tracks as $key => $track) {
            $tracks[$key] = 'spotify%3Atrack%3A'.$track;
        }
        $tracksString = implode(',', $tracks);
        $curl = 'curl -i -X POST "'.$url.$playlist['id'].'/tracks?uris='.$tracksString.'" -H "Authorization: Bearer '.AuthUtility::getAccessToken().'" -H "Accept: application/json" -H "Content-Length: 0"';

        return json_decode(shell_exec($curl), true);
    }
}