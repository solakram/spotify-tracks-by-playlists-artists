<?php

class PlaylistUtility
{

    public static function getPlaylists()
    {
        $user = UserUtility::getUser();
        $playlists = [];
        $url = 'https://api.spotify.com/v1/users/'.$user['id'].'/playlists';
        do {
            $response = CurlUtility::doRequest($url);
            $playlists = array_merge($playlists, $response['items']);
            $url = array_key_exists('next', $response) ? $response['next'] : null;
        } while ($url);

        return $playlists;
    }
}