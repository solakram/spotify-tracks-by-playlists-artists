<?php

class ArtistUtility
{

    public static function getArtistsByTracks($tracks)
    {
        $artists = [];
        foreach ($tracks as $track) {
            foreach ($track['track']['artists'] as $artist) {
                if (!array_key_exists($artist['id'], $artists)) {
                    $artists[$artist['id']] = ['artist' => $artist, 'tracks' => []];
                }
                $artists[$artist['id']]['tracks'][] = $track['track']['id'];
            }
        }
        foreach ($tracks as $track) { //handle all remix tracks
            if (strstr(strtolower($track['track']['name']), 'remix') !== false) {
                $artistName = ArtistUtility::parseArtistNameByRemixTrack($track['track']['name']);
                if ($artistName) {
                    $trackAddedToAnArtist = false;
                    foreach ($artists as $artistForRemixTrack) {
                        if ($artistForRemixTrack['artist']['name'] == $artistName) {
                            $artists[$artistForRemixTrack['artist']['id']]['tracks'][] = $track['track']['id'];
                            $trackAddedToAnArtist = true;
                            break;
                        }
                    }
                    if (!$trackAddedToAnArtist) {
                        $artists[$artistName] = [
                            'artist' => ['name' => $artistName, 'id' => $artistName],
                            'tracks' => [$track['track']['id']]
                        ];
                    }
                }
            }
        }

        return $artists;
    }

    private static function parseArtistNameByRemixTrack($trackName)
    {
        $artistName = '';
        if (strstr($trackName, ' - ') !== false) {
            $artistName = end(explode(" - ", $trackName));
        } elseif (strstr($trackName, '(') != false && strstr($trackName, ')') != false) {
            $artistName = end(explode("(", $trackName));
            $artistName = str_replace(')', '', $artistName);
        }

        return trim(str_replace('Remix', '', $artistName));
    }
}