<?php

class AuthUtility
{

    public static function doAuthTokenHandling() {
        try {
            AuthUtility::getAccessToken();
        } catch (\Exception $e) {
            if (array_key_exists('code', $_GET)) {
                AuthUtility::createAccessTokenResponseFromRequest();
            } else {
                AuthUtility::redirectToSpotifyAuthentication();
            }
        }
    }

    public static function getAccessToken()
    {
        $accessTokenResponse = SessionUtility::getAccessTokenResponse();
        if($accessTokenResponse['created_at'] + $accessTokenResponse['expires_in'] > time() && array_key_exists('access_token', $accessTokenResponse)) {
            return $accessTokenResponse['access_token'];
        } else {
            throw new \Exception('Access Token no longer valid');
        }
    }

    private static function createAccessTokenResponseFromRequest()
    {
        if (array_key_exists('code', $_GET)) {
            $tokenResult = CurlUtility::doTokenRequest($_GET['code']);
            $tokenResult['created_at'] = time();
            SessionUtility::saveAccessTokenResponse($tokenResult);
        }

        header('Location: '.ConfigUtility::getConfig()->getBaseUrl());
        exit();
    }

    private static function redirectToSpotifyAuthentication() {
        header('Location: '.AuthUtility::getAuthUrl());
        exit();
    }

    private static function getAuthUrl()
    {
        $config = ConfigUtility::getConfig();
        $redirectUri = urlencode($config->getBaseUrl());
        $scopes = [
            'user-read-email',
            'playlist-read-collaborative',
            'playlist-modify-private',
            'playlist-modify-public',
            'playlist-read-private',
        ];
        $scope = urlencode(implode((" "), $scopes));

        return 'https://accounts.spotify.com/authorize?client_id='.$config->getClientId().'&response_type=code&redirect_uri='.$redirectUri.'&scope='.$scope;
    }
}